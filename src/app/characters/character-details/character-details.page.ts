import { CharactersService } from './../characters.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { ICharacter } from '../characters.model';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.page.html',
  styleUrls: ['./character-details.page.scss'],
})
export class CharacterDetailsPage implements OnInit {

  character: ICharacter;

  constructor(private activatedRoute: ActivatedRoute, private charactersService: CharactersService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      flatMap((
        paramMap => this.charactersService.getCharacter(paramMap.get('characterId')
        )))
    ).subscribe( data => { 
      this.character = data;
      console.log(this.character);
    });
  } 

}