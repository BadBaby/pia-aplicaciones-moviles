export class Planet{
    constructor(
        public id: string,
        public name: string,
        public type: string,
        public dimension: string,
        public residents: string[],
        public url: string,
        public created: string
    ){}
}

export interface IPlanet{
    id: string;
    name: string;
    type: string;
    dimension: string;
    residents: string[];
    url: string;
    created: string;
}