import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Planet } from './planets.model';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  planets: Planet[];
  apiURL = 'https://rickandmortyapi.com/api/location/';

  constructor(private http: HttpClient) { }

  getURL(): Observable<any>{
    return this.http.get<any>(`${this.apiURL}`)
    .pipe(map(data => data.results as any));
  }

  getPlanets() {
    this.getURL().subscribe(res => {
      this.planets = res.results;
      console.log(this.planets);
    });
    return this.planets
  }

  getPlanet(planetId: string): Observable<Planet> {
    return this.http.get<any>(`${this.apiURL}/${planetId}/`)
    .pipe(map(data => data as Planet));
  }

}